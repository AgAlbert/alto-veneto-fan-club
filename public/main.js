let posts = [
    {
        img: "images/pic16.webP",
        title: "Teriomorfismo sarmatico",
        date: "19 Giugno 2024",
        desc: "Liberazione opinabile",
        link: "https://www.youtube.com/live/WXeSvnQK5HU?si=9bQYIE9pnEgHaswb&t=4770",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "https://i.ytimg.com/vi/G05okt4ZMAY/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAis2NsKYR0RED8B3UrGfCUXNoEjg",
        title: "Ma come parli bene",
        date: "17 Ottobre 2023",
        desc: "Ma ce t pinz ca stam a Rai 1? Ca qua a u TG Nuestre stam",
        link: "https://youtu.be/G05okt4ZMAY?feature=shared&t=21",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "images/pic15.webP",
        title: "Devi fare una pubblica PENETRAZIONE",
        date: "17 Giugno 2023",
        desc: "Noi russi già siamo divisi, ma…",
        link: "https://youtu.be/AVc46hgUoiY?t=3665",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "https://i.ytimg.com/vi/_KJ2pFn1NsM/hqdefault.jpg",
        title: "Bestemmione teatrale",
        date: "23 Marzo 2023",
        desc: "Siamo invidiati dall'estero anche per questo",
        link: "https://www.youtube.com/watch?v=_KJ2pFn1NsM",
        avatar: "images/avatar-ettore.webP"
    },
    {
        img: "images/pic14.webP",
        title: "A livello di economia…",
        date: "02 Gennaio 2023",
        desc: "Non sarebbe meglio un sistema socialista?",
        link: "https://youtu.be/ZzIX6I873E0?t=2471",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "images/pic13.webP",
        title: "S'è ammazzato in malo Mo-do",
        date: "02 Gennaio 2023",
        desc: "Immer weiter, ohne ende",
        link: "https://youtu.be/1y-6gexqJwo",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "images/pic12.webP",
        title: "Attenzione a Negro…",
        date: "07 Dicembre 2022",
        desc: "scusate AAAAAAAAAAA Greco",
        link: "https://youtu.be/LJpGnFYN6o0",
        avatar: "images/avatar-ettore.webP"
    },
    {
        img: "images/pic09.webP",
        title: "Dopo de onji Aperol, de Proseco",
        date: "29 Novembre 2022",
        desc: "Vojo anca un goto de acua onta",
        link: "https://www.youtube.com/watch?v=tK4wcRebs7E",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "images/pic11.webp",
        title: "Capra assassina Killer",
        date: "20 Novembre 2022",
        desc: "Abele non paga per Caino!",
        link: "https://www.youtube.com/watch?v=1qFd_zghhjw",
        avatar: "images/avatar-marcello.webP"
    },
    {
        img: "images/pic10.webP",
        title: "Devo metterti in riga!",
        date: "20 Novembre 2022",
        desc: "Non è una Veneta Elevata perciò deve essere messa in riga!",
        link: "https://www.youtube.com/watch?v=nl-zIL2o9Nc",
        avatar: "images/avatar-ettore.webP"
    },
    {
        img: "images/pic01.webP",
        title: "Uno dei sogni agognati",
        date: "16 Novembre 2022",
        desc: "È un uomo, una donna, è tutto!<p/> Una vacca assassina killer!",
        link: "https://www.dailymotion.com/video/x14as1o",
        avatar: "images/avatar-marcello.webP"
    }
];

let grid = document.querySelector("#grid");

for (let post of posts) {
    grid.innerHTML += 
    `<section class="box feature">
        <a href="${post.link}" class="image featured"><img src="${post.img}" alt="" /></a>
        <div class="inner">
            <header>
                <h2><a href="${post.link}">${post.title}</a></h2>
                <p>${post.date}</p>
            </header>
            <p class="desc">${post.desc}</p>
            <img class="avatar" src="${post.avatar}"></img>
        </div>
    </section>`
}
